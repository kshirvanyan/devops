from node:8.12.0-alpine
workdir /app
copy main.js /app
copy package.json /app
copy package-lock.json /app
expose 5000
run npm install
entrypoint ["node","main.js"]
